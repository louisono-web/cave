{-
  Random 2D cave generation.

  author: louisono
-}

module Cave exposing (..)


import Html exposing (Html, div, p, button, span, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Random
import Array2D exposing (Array2D, Size2D, initXY, mapXY, getAll, neighbors)
import Set
import Browser exposing (element)


-- CaveTile
-- type used to represent each position of a cave


type CaveTile = 
  Wall
  | Ground


type alias Cave = Array2D CaveTile


-- Generate Cave


-- single iteration of the cave generation
ageCave: Cave -> Cave
ageCave cave =
  let
    wallCondition tile count =
      if count >= 5
      then Wall
      else if count <= 2
      then Ground
      else tile
  in
  mapXY
    (\p centerTile ->
      neighbors p cave
      |> Set.toList
      |> (\positionList -> getAll positionList cave)
      |> List.foldl (\(_, tile) wallCount -> if tile == Wall then wallCount + 1 else wallCount) 0
      |> (\wallCount -> if centerTile == Wall then wallCount + 1 else wallCount)
      |> wallCondition centerTile
    )
    cave


-- Display


type alias Model = {cave: Cave, logs: List Msg}


{- signature:
    element :
      { init : flags -> ( model, Cmd msg )          -- ok, done
      , view : model -> Html msg                    -- ok, done
      , update : msg -> model -> ( model, Cmd msg ) -- ok, done
      , subscriptions : model -> Sub msg            -- todo
      }
-}


-- constants
caveSize = Size2D 60 30
initialAge = 50
nonCave = initXY (Size2D 0 0) (\_ -> Wall)


repeat: (a -> a) -> Int -> a -> a
repeat f times val =
  if times <= 0
  then val
  else repeat f (times - 1) (f val)


type Msg =
  NewSeed
  | Initialize (List CaveTile)
  | Age


msgToStr: Msg -> String
msgToStr msg =
  case msg of
    NewSeed -> "NewSeed"
    Initialize _ -> "Initialize"
    Age -> "Age"


encloseCave: Cave -> Cave
encloseCave ({size} as cave) =
  let 
    {width, height} = size
  in
  mapXY
    (\(x, y) tile -> 
      if x == 0 || y == 0 || x == (width-1) || y == (height-1)
      then Wall
      else tile
    )
    cave


initCmd: Cmd Msg
initCmd =
  let
    weightedTiles =
      Random.weighted
        (3, Wall)
        [ (7, Ground) ]
  in
  Random.generate 
    Initialize
    (Random.list (caveSize.width * caveSize.height) weightedTiles)


init: () -> (Model, Cmd Msg)
init _ =
  ( {cave=nonCave, logs=[]}
  , initCmd
  )


viewCaveTile: CaveTile -> Html Msg
viewCaveTile tile =
  case tile of
    Wall -> span [ class "tile-wall" ] [ text "#" ]
    Ground -> span [ class "tile-ground" ] [ text "." ]


viewCave: Cave -> Html Msg
viewCave cave =
  let
    viewRow r =
      List.map viewCaveTile r
      |> div [ class  "cave-row" ]
  in
  Array2D.rows cave
  |> List.map viewRow
  |> div [ class "cave" ]


view: Model -> Html Msg
view {cave, logs} =
  let 
    caveDiv = viewCave cave
    ageButton = button [ onClick Age ] [ text "age that cave" ]
    restartButton = button [ onClick NewSeed ] [ text "start over" ]
    logsDiv = List.map (msgToStr >> (\a -> [text a]) >> p []) logs |> div [ class "logs" ]
  in
  div [ class "app" ] [ caveDiv, ageButton, restartButton, logsDiv ]


update: Msg -> Model -> (Model, Cmd Msg)
update msg {cave, logs} =
  case msg of
    NewSeed ->
      ( {cave=cave, logs=msg::logs}
      , initCmd
      )
    Initialize caveTiles ->
      let
        initializedCave = 
          Array2D.fromList caveSize caveTiles 
          |> Maybe.withDefault nonCave
          |> encloseCave
      in
      ( {cave=initializedCave, logs=msg::logs}
      , Cmd.none
      )
    Age ->
      let agedCave = ageCave cave 
      in
      ( {cave=agedCave, logs=msg::logs}
      , Cmd.none
      )


subscriptions: Model -> Sub Msg
subscriptions _ = Sub.none


main =
  element
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }
