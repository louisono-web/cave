module Array2D exposing (..)


import Array exposing (Array)
import Set exposing (Set)
import Dict exposing (Dict)



type alias Size2D = {width: Int, height: Int}


type alias Array2D a = {size: Size2D, arr: Array a}


type alias Position = (Int, Int)


-- Casting


strOfSize2D: Size2D -> String
strOfSize2D {width, height} =
  String.concat [String.fromInt width, " x ", String.fromInt height]


strOfPosition: Position -> String
strOfPosition (x, y) =
  String.concat ["Position", "(x=", String.fromInt x, ", y=", String.fromInt y, ")"]


rows: Array2D a -> List (List a)
rows ({size} as arr2d) =
  let
    rowReducer cell {currentRow, rowList, count} =
      let
        startOfNewRow = count /= 0 && remainderBy size.width count == 0
        r = if startOfNewRow then [cell] else (cell :: currentRow)
        rs = if startOfNewRow then ((List.reverse currentRow) :: rowList) else rowList
        c = count + 1
      in
      {currentRow = r, rowList = rs, count = c}
  in
  fold rowReducer {currentRow = [], rowList = [], count = 0} arr2d
  |> (\{currentRow, rowList} -> (List.reverse currentRow) :: rowList)
  |> List.reverse


fromList: Size2D -> List a -> Maybe (Array2D a)
fromList ({width, height} as size) list =
  let
    feed ((x, y) as pos) li arr =
      case li of
        [] -> arr
        element :: rest ->
          if y == height
          then arr
          else if x == width
          then feed (0, (y+1)) li arr
          else feed ((x+1), y) rest (set pos element arr)
  in
  case list of
    [] -> Nothing
    head :: tail ->
      initXY size (\_ -> head)
      |> feed (0, 0) list
      |> Maybe.Just


-- Access


get: Position -> Array2D a -> Maybe a
get pos {size, arr} =
  Array.get (posToI size.width pos) arr


getAll: List Position -> Array2D a -> List (Position, a)
getAll positions arr2d =
  let
    zipPosEl p =
      case (get p arr2d) of
        Nothing -> Nothing
        Just el -> Just (p, el)
  in
  List.filterMap zipPosEl positions


-- Create


positionXY: Int -> Int -> Position
positionXY x y = (x, y)


initXY: Size2D -> (Position -> a) -> Array2D a
initXY ({width, height} as size) initializer =
  let
    arr = Array.initialize (width*height) ((iToPos width) >> initializer)
  in
  Array2D size arr


-- Transform


set: Position -> a -> Array2D a -> Array2D a
set pos val ({size, arr} as arr2d) =
  {arr2d | arr = Array.set (posToI size.width pos) val arr}


map: (a -> b) -> Array2D a -> Array2D b
map mapper ({size, arr} as arr2d) =
  let
    newArr = Array.map mapper arr
  in
  Array2D size newArr


mapArea: (a -> a) -> Set Position -> Array2D a -> Array2D a
mapArea mapper positions =
  let
    localizedMapper =
      (\pos el -> if Set.member pos positions then mapper el else el)
  in
  mapXY localizedMapper


mapXY: (Position -> a -> b) -> Array2D a -> Array2D b
mapXY mapper ({size, arr} as arr2d) =
  let
    newArr = Array.indexedMap ((iToPos size.width) >> mapper) arr
  in
  Array2D size newArr


fold: (a -> b -> b) -> b -> Array2D a -> b
fold reducer init =
  (.arr) >> Array.foldl reducer init


propagate: (a -> Bool) -> (a -> a) -> Position -> Array2D a -> Array2D a
propagate filter mapper pos arr2d =
  let
    area = explore filter pos arr2d
  in
  mapXY (\position el -> if Set.member position area then mapper el else el) arr2d


-- Utils / helpers


neighbors: Position -> Array2D a -> Set Position
neighbors (x, y) {size} =
  [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]
  |> List.filterMap (\(dx, dy) -> withinBounds size (positionXY (x+dx) (y+dy)))
  |> Set.fromList


directNeighbors: Position -> Array2D a -> Set Position
directNeighbors (x, y) {size} =
  [(-1, 0), (1, 0), (0, -1), (0, 1)]
  |> List.filterMap (\(dx, dy) -> withinBounds size (positionXY (x+dx) (y+dy)))
  |> Set.fromList


expandBorder: Set Position -> Array2D a -> Set Position
expandBorder area arr2d =
  Set.toList area
  |> List.map (\pos -> neighbors pos arr2d)
  |> List.foldl (Set.union) area


exploreBorder: (a -> Bool) -> Set Position -> Array2D a -> Set Position
exploreBorder filter area arr2d =
  let
    filterP pos =
      get pos arr2d
      |> Maybe.map filter
      |> Maybe.withDefault False
  in
  Set.toList area
  |> List.map (\pos -> neighbors pos arr2d)
  |> List.foldl (Set.union) area
  |> Set.filter filterP


explore: (a -> Bool) -> Position -> Array2D a -> Set Position
explore filter pos arr2d =
  let
    iter area =
      let exploredArea = exploreBorder filter area arr2d
      in
      if exploredArea == area then
        area
      else 
        iter exploredArea
  in
  if get pos arr2d |> Maybe.map filter |> Maybe.withDefault False then
    iter (Set.singleton pos)
    |> (\s -> expandBorder s arr2d)
  else
    Set.singleton pos


posToI: Int -> Position -> Int
posToI width (x, y) =
  width * y + x


iToPos: Int -> Int -> Position
iToPos width index =
  let
    x = remainderBy width index
    y = index // width
  in
  positionXY x y


outOfBounds: Size2D -> Position -> Bool
outOfBounds {width, height} (x, y) =
  x < 0 || x >= width || y < 0 || y >= height


withinBounds: Size2D -> Position -> Maybe Position
withinBounds size pos =
  if outOfBounds size pos then Nothing else Just pos
