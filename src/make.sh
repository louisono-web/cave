#! /usr/bin/env bash
THIS_DIR=$(dirname $0)
SOURCE="$THIS_DIR/Cave.elm"
TARGET="$THIS_DIR/../compiled-cave.js"

elm make $SOURCE --output $TARGET
