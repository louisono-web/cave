# Cave
An elm implementation of a random cave generator, that could be used to create cavern-like levels for games.

## Algorithm
The cave generator is a cellular automaton based algorithm described in details on [roguebasin](https://www.roguebasin.com/index.php?title=Cellular_Automata_Method_for_Generating_Random_Cave-Like_Levels).

## Build & run

### Prerequisite
- To build the app, the [elm compiler](https://github.com/elm/compiler) is required
- To run the compiled-to-javascript code, you just need a web browser (pretty much any modern web browser).

### Build
A bash script is available in the `src` folder: [`make.sh`](./src/make.sh). Run that script from the root folder of the project to generate `compiled-cave.js`: the javascript file containing the app logic.

### Run
Use your favorite web browser to open the `index.html` file.
